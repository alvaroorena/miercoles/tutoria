<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresProfesionales */

$this->title = $model->codigo_jugadores_profesionales;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores Profesionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="jugadores-profesionales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->codigo_jugadores_profesionales], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->codigo_jugadores_profesionales], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_jugadores_profesionales',
            'dni_jugadores_profesionales',
            'nombre',
            'fecha_nacimiento',
            'posicion',
            'club_procedencia',
            'goles',
            'asistencias',
            'goles_generados',
            'tarjetas_amarillas',
            'tarjetas_rojas',
            'salario_bruto',
            'numero_cuenta',
            'codigo_directivo',
        ],
    ]) ?>

</div>
