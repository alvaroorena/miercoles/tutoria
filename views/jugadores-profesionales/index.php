<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jugadores Profesionales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-profesionales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Jugadores Profesionales', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo_jugadores_profesionales',
            'dni_jugadores_profesionales',
            'nombre',
            'fecha_nacimiento',
            'posicion',
            //'club_procedencia',
            //'goles',
            //'asistencias',
            //'goles_generados',
            //'tarjetas_amarillas',
            //'tarjetas_rojas',
            //'salario_bruto',
            //'numero_cuenta',
            //'codigo_directivo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
