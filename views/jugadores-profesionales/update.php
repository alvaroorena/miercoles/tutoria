<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresProfesionales */

$this->title = 'Update Jugadores Profesionales: ' . $model->codigo_jugadores_profesionales;
$this->params['breadcrumbs'][] = ['label' => 'Jugadores Profesionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_jugadores_profesionales, 'url' => ['view', 'id' => $model->codigo_jugadores_profesionales]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jugadores-profesionales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
