<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lesiones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primer_apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'segundo_apellido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_lesion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_lesion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plan_recuperacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_alta')->textInput() ?>

    <?= $form->field($model, 'fecha_baja')->textInput() ?>

    <?= $form->field($model, 'tiempo_estimado_recuperacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_jugadores_profesionales')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
