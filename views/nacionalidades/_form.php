<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nacionalidades-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nacionalidades')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_jugadores_profesionales')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
