<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ingresos */

$this->title = 'Create Ingresos';
$this->params['breadcrumbs'][] = ['label' => 'Ingresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingresos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
