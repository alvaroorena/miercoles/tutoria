<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ingresos */

$this->title = 'Update Ingresos: ' . $model->codigo_ingreso;
$this->params['breadcrumbs'][] = ['label' => 'Ingresos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_ingreso, 'url' => ['view', 'id' => $model->codigo_ingreso]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ingresos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
