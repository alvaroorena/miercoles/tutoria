<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reservas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_reserva')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nacimiento')->textInput() ?>

    <?= $form->field($model, 'posicion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'club_actual')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'goles')->textInput() ?>

    <?= $form->field($model, 'asistencias')->textInput() ?>

    <?= $form->field($model, 'goles_generados')->textInput() ?>

    <?= $form->field($model, 'salario_bruto')->textInput() ?>

    <?= $form->field($model, 'codigo_staff')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
