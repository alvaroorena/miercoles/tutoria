<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JugadoresNoProfesionales */

$this->title = 'Create Jugadores No Profesionales';
$this->params['breadcrumbs'][] = ['label' => 'Jugadores No Profesionales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jugadores-no-profesionales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
