<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directivos".
 *
 * @property int $codigo_directivo
 * @property string|null $dni_directivo
 * @property string|null $nombre
 * @property string|null $primer_apellido
 * @property string|null $segundo_apellido
 * @property string|null $telefono
 * @property string|null $cargo
 *
 * @property Empleados[] $empleados
 * @property Entrenadores[] $entrenadores
 * @property Gastos[] $gastos
 * @property Ingresos[] $ingresos
 * @property JugadoresProfesionales[] $jugadoresProfesionales
 * @property Recursos[] $recursos
 * @property Staff[] $staff
 */
class Directivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_directivo'], 'string', 'max' => 9],
            [['nombre', 'primer_apellido', 'segundo_apellido', 'cargo'], 'string', 'max' => 20],
            [['telefono'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_directivo' => 'Codigo Directivo',
            'dni_directivo' => 'Dni Directivo',
            'nombre' => 'Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'telefono' => 'Telefono',
            'cargo' => 'Cargo',
        ];
    }

    /**
     * Gets query for [[Empleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gastos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Ingresos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngresos()
    {
        return $this->hasMany(Ingresos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[JugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresProfesionales()
    {
        return $this->hasMany(JugadoresProfesionales::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Recursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos()
    {
        return $this->hasMany(Recursos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }

    /**
     * Gets query for [[Staff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(Staff::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
