<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidades".
 *
 * @property int $id_nacionalidades
 * @property string|null $nacionalidades
 * @property int|null $codigo_jugadores_profesionales
 *
 * @property JugadoresProfesionales $codigoJugadoresProfesionales
 */
class Nacionalidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_jugadores_profesionales'], 'integer'],
            [['nacionalidades'], 'string', 'max' => 30],
            [['nacionalidades', 'codigo_jugadores_profesionales'], 'unique', 'targetAttribute' => ['nacionalidades', 'codigo_jugadores_profesionales']],
            [['codigo_jugadores_profesionales'], 'exist', 'skipOnError' => true, 'targetClass' => JugadoresProfesionales::className(), 'targetAttribute' => ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nacionalidades' => 'Id Nacionalidades',
            'nacionalidades' => 'Nacionalidades',
            'codigo_jugadores_profesionales' => 'Codigo Jugadores Profesionales',
        ];
    }

    /**
     * Gets query for [[CodigoJugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadoresProfesionales()
    {
        return $this->hasOne(JugadoresProfesionales::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }
}
