<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staff".
 *
 * @property int $codigo_staff
 * @property string|null $dni_staff
 * @property string|null $nombre
 * @property string|null $puesto
 * @property float|null $salario_bruto
 * @property string|null $numero_cuenta
 * @property int|null $codigo_directivo
 *
 * @property Reservas[] $reservas
 * @property Directivos $codigoDirectivo
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salario_bruto'], 'number'],
            [['codigo_directivo'], 'integer'],
            [['dni_staff'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 100],
            [['puesto'], 'string', 'max' => 20],
            [['numero_cuenta'], 'string', 'max' => 24],
            [['codigo_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['codigo_directivo' => 'codigo_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_staff' => 'Codigo Staff',
            'dni_staff' => 'Dni Staff',
            'nombre' => 'Nombre',
            'puesto' => 'Puesto',
            'salario_bruto' => 'Salario Bruto',
            'numero_cuenta' => 'Numero Cuenta',
            'codigo_directivo' => 'Codigo Directivo',
        ];
    }

    /**
     * Gets query for [[Reservas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reservas::className(), ['codigo_staff' => 'codigo_staff']);
    }

    /**
     * Gets query for [[CodigoDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
