<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores_no_profesionales".
 *
 * @property int $codigo_jugadores_no_profesionales
 * @property string|null $dni_jugadores_no_profesionales
 * @property string|null $nombre
 * @property string|null $primer_apellido
 * @property string|null $segundo_apellido
 * @property string|null $fecha_nacimiento
 * @property int|null $edad
 * @property int|null $material_prestado
 * @property string|null $nombre_p_m_t
 * @property string|null $dni_p_m_t
 * @property string|null $primer_apellido_p_m_t
 * @property string|null $segundo_apellido_p_m_t
 * @property string|null $numero_cuenta_p_m_t
 * @property string|null $codigo_categoria
 *
 * @property CategoriasInferiores $codigoCategoria
 */
class JugadoresNoProfesionales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores_no_profesionales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_nacimiento'], 'safe'],
            [['edad', 'material_prestado'], 'integer'],
            [['dni_jugadores_no_profesionales', 'dni_p_m_t'], 'string', 'max' => 9],
            [['nombre', 'primer_apellido', 'segundo_apellido', 'nombre_p_m_t', 'primer_apellido_p_m_t', 'segundo_apellido_p_m_t'], 'string', 'max' => 20],
            [['numero_cuenta_p_m_t'], 'string', 'max' => 24],
            [['codigo_categoria'], 'string', 'max' => 5],
            [['codigo_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriasInferiores::className(), 'targetAttribute' => ['codigo_categoria' => 'codigo_categoria']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_jugadores_no_profesionales' => 'Codigo Jugadores No Profesionales',
            'dni_jugadores_no_profesionales' => 'Dni Jugadores No Profesionales',
            'nombre' => 'Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'edad' => 'Edad',
            'material_prestado' => 'Material Prestado',
            'nombre_p_m_t' => 'Nombre P M T',
            'dni_p_m_t' => 'Dni P M T',
            'primer_apellido_p_m_t' => 'Primer Apellido P M T',
            'segundo_apellido_p_m_t' => 'Segundo Apellido P M T',
            'numero_cuenta_p_m_t' => 'Numero Cuenta P M T',
            'codigo_categoria' => 'Codigo Categoria',
        ];
    }

    /**
     * Gets query for [[CodigoCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCategoria()
    {
        return $this->hasOne(CategoriasInferiores::className(), ['codigo_categoria' => 'codigo_categoria']);
    }
}
