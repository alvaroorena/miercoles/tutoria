<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias_inferiores".
 *
 * @property string $codigo_categoria
 * @property string|null $nombre
 * @property string|null $rango
 * @property int|null $numero_jugadores
 * @property string|null $proximo_partido
 * @property int|null $codigo_entrenador
 *
 * @property Entrenadores $codigoEntrenador
 * @property JugadoresNoProfesionales[] $jugadoresNoProfesionales
 */
class CategoriasInferiores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias_inferiores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_categoria'], 'required'],
            [['numero_jugadores', 'codigo_entrenador'], 'integer'],
            [['codigo_categoria', 'rango'], 'string', 'max' => 5],
            [['nombre'], 'string', 'max' => 20],
            [['proximo_partido'], 'string', 'max' => 90],
            [['codigo_categoria'], 'unique'],
            [['codigo_entrenador'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['codigo_entrenador' => 'codigo_entrenador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_categoria' => 'Codigo Categoria',
            'nombre' => 'Nombre',
            'rango' => 'Rango',
            'numero_jugadores' => 'Numero Jugadores',
            'proximo_partido' => 'Proximo Partido',
            'codigo_entrenador' => 'Codigo Entrenador',
        ];
    }

    /**
     * Gets query for [[CodigoEntrenador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEntrenador()
    {
        return $this->hasOne(Entrenadores::className(), ['codigo_entrenador' => 'codigo_entrenador']);
    }

    /**
     * Gets query for [[JugadoresNoProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadoresNoProfesionales()
    {
        return $this->hasMany(JugadoresNoProfesionales::className(), ['codigo_categoria' => 'codigo_categoria']);
    }
}
