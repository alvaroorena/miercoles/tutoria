<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ingresos".
 *
 * @property int $codigo_ingreso
 * @property string|null $motivo
 * @property float|null $importe
 * @property string|null $fecha
 * @property int|null $codigo_directivo
 *
 * @property Directivos $codigoDirectivo
 */
class Ingresos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingresos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['importe'], 'number'],
            [['fecha'], 'safe'],
            [['codigo_directivo'], 'integer'],
            [['motivo'], 'string', 'max' => 40],
            [['codigo_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['codigo_directivo' => 'codigo_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_ingreso' => 'Codigo Ingreso',
            'motivo' => 'Motivo',
            'importe' => 'Importe',
            'fecha' => 'Fecha',
            'codigo_directivo' => 'Codigo Directivo',
        ];
    }

    /**
     * Gets query for [[CodigoDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
