<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $id_juegan
 * @property int|null $codigo_partido
 * @property int|null $codigo_jugadores_jugadores_profesionales
 *
 * @property JugadoresProfesionales $codigoJugadoresJugadoresProfesionales
 * @property PartidosOficiales $codigoPartido
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_partido', 'codigo_jugadores_jugadores_profesionales'], 'integer'],
            [['codigo_partido', 'codigo_jugadores_jugadores_profesionales'], 'unique', 'targetAttribute' => ['codigo_partido', 'codigo_jugadores_jugadores_profesionales']],
            [['codigo_jugadores_jugadores_profesionales'], 'exist', 'skipOnError' => true, 'targetClass' => JugadoresProfesionales::className(), 'targetAttribute' => ['codigo_jugadores_jugadores_profesionales' => 'codigo_jugadores_profesionales']],
            [['codigo_partido'], 'exist', 'skipOnError' => true, 'targetClass' => PartidosOficiales::className(), 'targetAttribute' => ['codigo_partido' => 'codigo_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_juegan' => 'Id Juegan',
            'codigo_partido' => 'Codigo Partido',
            'codigo_jugadores_jugadores_profesionales' => 'Codigo Jugadores Jugadores Profesionales',
        ];
    }

    /**
     * Gets query for [[CodigoJugadoresJugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadoresJugadoresProfesionales()
    {
        return $this->hasOne(JugadoresProfesionales::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_jugadores_profesionales']);
    }

    /**
     * Gets query for [[CodigoPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido()
    {
        return $this->hasOne(PartidosOficiales::className(), ['codigo_partido' => 'codigo_partido']);
    }
}
