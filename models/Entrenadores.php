<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $codigo_entrenador
 * @property string|null $dni_entrenadores
 * @property string|null $nombre
 * @property string|null $primer_apellido
 * @property string|null $segundo_apellido
 * @property string|null $licencia
 * @property string|null $categoría
 * @property string|null $teléfono
 * @property int|null $codigo_directivo
 *
 * @property CategoriasInferiores[] $categoriasInferiores
 * @property Directivos $codigoDirectivo
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_directivo'], 'integer'],
            [['dni_entrenadores'], 'string', 'max' => 9],
            [['nombre', 'primer_apellido', 'segundo_apellido', 'licencia', 'categoría'], 'string', 'max' => 20],
            [['teléfono'], 'string', 'max' => 12],
            [['codigo_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['codigo_directivo' => 'codigo_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_entrenador' => 'Codigo Entrenador',
            'dni_entrenadores' => 'Dni Entrenadores',
            'nombre' => 'Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'licencia' => 'Licencia',
            'categoría' => 'Categoría',
            'teléfono' => 'Teléfono',
            'codigo_directivo' => 'Codigo Directivo',
        ];
    }

    /**
     * Gets query for [[CategoriasInferiores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriasInferiores()
    {
        return $this->hasMany(CategoriasInferiores::className(), ['codigo_entrenador' => 'codigo_entrenador']);
    }

    /**
     * Gets query for [[CodigoDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['codigo_directivo' => 'codigo_directivo']);
    }
}
