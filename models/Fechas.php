<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fechas".
 *
 * @property int $id_fecha
 * @property string|null $fecha
 * @property int|null $codigo_jugadores_profesionales
 *
 * @property JugadoresProfesionales $codigoJugadoresProfesionales
 */
class Fechas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fechas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['codigo_jugadores_profesionales'], 'integer'],
            [['codigo_jugadores_profesionales'], 'exist', 'skipOnError' => true, 'targetClass' => JugadoresProfesionales::className(), 'targetAttribute' => ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_fecha' => 'Id Fecha',
            'fecha' => 'Fecha',
            'codigo_jugadores_profesionales' => 'Codigo Jugadores Profesionales',
        ];
    }

    /**
     * Gets query for [[CodigoJugadoresProfesionales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoJugadoresProfesionales()
    {
        return $this->hasOne(JugadoresProfesionales::className(), ['codigo_jugadores_profesionales' => 'codigo_jugadores_profesionales']);
    }
}
